import sbt._
import sbt.Keys._

object ScalaProjectBuild extends Build {

  // Change _name to your project name.
  val _name = "adbrain-dataprovider-evaluation"
  val _version = "1.0.1"
  val _scalaVersion = "2.11.8"

  val sparkVersion = "2.0.0"
  val mortbayJetty = ExclusionRule(organization = "org.mortbay.jetty")
  val servletApi   = ExclusionRule(organization = "javax.servlet")


  lazy val scalaProject = Project(
    id = _name,
    base = file("."),

    settings =
      Defaults.coreDefaultSettings ++
        net.virtualvoid.sbt.graph.Plugin.graphSettings ++ Seq(
        name := _name,
        version := _version,
        scalaVersion := _scalaVersion,

        // To exclude generation of src/main/java, src/test/java
        unmanagedSourceDirectories in Compile <<= baseDirectory(base => (base / "src" / "main" / "scala") :: Nil),
        unmanagedSourceDirectories in Test <<= baseDirectory(base => (base / "src" / "test" / "scala") :: Nil),

        // Must run the examples and tests in separate JVMs to avoid mysterious
        // scala.reflect.internal.MissingRequirementError errors.
        // (https://issues.apache.org/jira/browse/SPARK-5281)
        // This should be removed when fixed in Spark SQL.
        fork := true,

        parallelExecution in Test := false,

        // Need to exclude slf4j-log4j12for logback to work.
        ivyXML :=
          <dependencies>
            <exclude org="org.slf4j" name="slf4j-log4j12"/>
          </dependencies>,

        // To remove warning about different versions of Scala (2.10.4, 2.10.5)
        ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) },

        libraryDependencies ++= Seq(

          "org.apache.spark"       %% "spark-core"                % sparkVersion % "provided"
            excludeAll(servletApi, mortbayJetty),
          "org.apache.spark"        %% "spark-sql"                 % sparkVersion % "provided",
          "org.apache.spark"        %% "spark-hive"                % sparkVersion % "provided",
          "org.joda"                 % "joda-convert"              % "1.7",
          "joda-time"                % "joda-time"                 % "2.7",
          "org.apache.hadoop"        % "hadoop-aws"                % "2.6.3"
            exclude("com.fasterxml.jackson.core", "jackson-core")
            exclude("com.fasterxml.jackson.core", "jackson-databind")
            exclude("com.fasterxml.jackson.core", "jackson-annotations"),
          "org.xerial.snappy"        % "snappy-java"               % "1.1.1.6",
          "org.scalatest"           %% "scalatest"                 % "2.2.4"      % "test"
        )

      )
  )
    .settings(SCoverage.settings: _*)
    .settings(Assembly.settings: _*)
}
