resolvers += "Adbrain libs-releases" at "https://adbrainai.artifactoryonline.com/adbrainai/libs-releases"

credentials += Credentials("Artifactory Realm", "adbrainai.artifactoryonline.com", "ai", "Art1f4ctory")


addSbtPlugin("com.adbrain"              % "sbt-build"            % "1.0.3")

addSbtPlugin("com.typesafe.sbteclipse"  % "sbteclipse-plugin"    % "2.4.0")

addSbtPlugin("com.eed3si9n"             % "sbt-assembly"         % "0.11.2")

addSbtPlugin("net.virtual-void"         % "sbt-dependency-graph" % "0.7.5")

addSbtPlugin("org.scoverage"            % "sbt-scoverage"        % "1.0.1")
