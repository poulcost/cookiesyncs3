package com.adbrain.avrowrapper

import scala.collection.TraversableOnce
import scala.util.Try
import java.io.IOException

import org.apache.avro.generic.GenericRecord
import org.apache.avro.mapred.AvroKey
import org.apache.avro.{AvroRuntimeException, Schema}
import org.apache.avro.mapreduce.{AvroJob, AvroKeyRecordReader}
import org.apache.hadoop.io.NullWritable
import org.apache.hadoop.mapreduce.{InputSplit, TaskAttemptContext}
import org.apache.hadoop.mapreduce.lib.input._
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.functions._
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.joda.time.format.DateTimeFormat
import org.joda.time.{LocalDate, Period}

/**
  * Created by poulcostinsky on 7/21/16.
  */

class AvroSkipKeyRecordReader[T](schema: Schema) extends AvroKeyRecordReader[T](schema) {

  override def nextKeyValue(): Boolean = try {
    super.nextKeyValue()
  } catch {
    case _: AvroRuntimeException | _ : IOException => false
  }

}

/**
  * A custom format to skip corrupted Avro files.
  */
class AvroSkipKeyInputFormat[T] extends FileInputFormat[AvroKey[T], NullWritable] {

  def createRecordReader(split: InputSplit, context: TaskAttemptContext) = {
    val schema = AvroJob.getInputKeySchema(context.getConfiguration)
    new AvroSkipKeyRecordReader(schema)
  }
}

/**
  * overcoming some shortcomings of databricks' avro library.
  */
object avro {

  /**
    *   Reads avro file or files, where
    *   - corrupt files are skipped
    *   - columns whose type is not supported by SparkSql are ignored, instead of throwing an exception.
    *   - date ranges are checked

    * @param schema which fields to extract to the dataFrame
    * @param dateFrom starting day of the date range
    * @param dateTo   ending day of the date range, inclusive
    * @param failOnEmptyDirectories if false, empty or corrupt directories will be skipped. For example, missing dates.
    * @param dirs list of direcrories
    * @param spark implicit spark session
    * @return DataFrame, union of all the files
    */
    def readAvroFiles(schema: StructType, dateFrom: String, dateTo: String, failOnEmptyDirectories: Boolean, dirs: String*)
                     (implicit spark: SparkSession ) : DataFrame = {
      val conf = spark.sparkContext.hadoopConfiguration
      conf.setBoolean("mapreduce.input.fileinputformat.input.dir.recursive", true)
      conf.set("fs.s3.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem")
      conf.set("fs.s3n.awsAccessKeyId", "AKIAIDS27NYMILREHO5Q")
      conf.set("fs.s3n.awsSecretAccessKey", "foobar")

      // helper function - read one direcrory or file.
      def readDir (dir: (String, String)) = {
          val rdd =
            spark.sparkContext.newAPIHadoopFile(
              dir._2,
              classOf[AvroSkipKeyInputFormat[GenericRecord]],
              classOf[AvroKey[GenericRecord]],
              classOf[NullWritable],
              conf
            )
              .map(_._1.datum())

          // this forces the rdd read to fail early if the directory is empty or invalid,
          // so it can be caught in Try, so empty directories will be skipped.
          if (!failOnEmptyDirectories)
            rdd.partitions // TODO - check how it parallels

          // convert to SparkSql data frame, add the timestamp column from the directory name,
          createDataFrame(schema, rdd)
            .withColumn("last_update", to_date(lit(dir._1)))
      }

      val crossDirs = listDirs(dateFrom, dateTo, dirs :_*)
      val results =
        if (failOnEmptyDirectories)
          crossDirs.map(readDir(_))
        else
          crossDirs.map(dir => Try(readDir(dir)))
            .filter(_.isSuccess)
            .map(_.get)

      results.reduce(_ union _)
    }

  /**
    * Cross apply all the days in the range to list of directories.
    * @param dateFrom starting date
    * @param dateTo   last date (inclusuve)
    * @param dirs     direcrories.
    * @return         list of (date, dir) tuples of all possible combinations.
    */
    def listDirs(dateFrom: String, dateTo: String, dirs: String*) : TraversableOnce[(String, String)] = {
      val datePattern = DateTimeFormat.forPattern("yyyy-MM-dd")

      // create list of dates
      val dates =
        Iterator
          .iterate(LocalDate.parse(dateFrom))(_.plus(Period.days(1)))
          .takeWhile(!_.isAfter(LocalDate.parse(dateTo)))
          .map(datePattern.print(_))
          .toArray

      // cross apply list of directories to the list of dates
      dirs.flatMap(dir => dates.map(date => (date, dir + "/" + date + "/*.avro")))
    }

  /**
    * Maps RDD into dataframe, using provided schema
    * @param schema
    * @param rdd
    * @param spark
    * @return DataFrame
    */
    def createDataFrame (schema: StructType, rdd: RDD[GenericRecord])
                        (implicit spark: SparkSession) : DataFrame = {
      val rdd2 = rdd.map(rec => schema.map(field => rec.get(field.name)))   // map the fields we need
        .map(field => Row(field: _*))                                       // convert to Row - required for dataFrame

      spark.createDataFrame(rdd2, schema)
    }
}
