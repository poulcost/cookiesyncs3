package com.adbrain.cookieSyncIngress

import org.apache.spark.sql._
import com.adbrain.avrowrapper.avro
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.joda.time.format.DateTimeFormat
import org.joda.time.{LocalDate, LocalDateTime}

/**
  * Created by poulcostinsky on 7/22/16.
  */
object CookieSyncImporter {
  def Import(outputPath: String, jumpStart: Boolean, cutOffDays: Int, dateFrom: String, dateTo: String,  failOnEmptyDirectories: Boolean, dirs: String*)(implicit spark: SparkSession)  : Unit = {

      val p2aOutputPath = outputPath + "/P2A"

      val newDataFull = readCookieSyncLogs(
            dateFrom,
            dateTo,
            failOnEmptyDirectories,
            dirs: _*)
          .select("syncid", "pn_cid", "ab_cid", "last_update")

      // it seems to work much faster if the new data is deduped separately:
      // there's much less records, but much more duplicates
      val newData =
        if (dateFrom == dateTo)
          dedupeSingleDate(newDataFull)
        else
          dedupe(newDataFull)

      // the old data will not be there the very first time.
      var oldDataSize: Long = 0
      val deduped =
        if (jumpStart) {
            newData   // already deduped
          }
        else {
          // read the current state
          val data = spark.read.parquet(p2aOutputPath + "/Latest")

          // use this opportunity to age away old cookies
          val oldData = data
            .select("syncid", "pn_cid", "ab_cid", "last_update")
            .where(
              date_add(to_date(data("last_update")), cutOffDays)
              .gt(current_date())
            )

          oldDataSize = oldData.count()
          // this will deduplicate old and new data togetehr.
          // since both are already separately deduped at this point,
          // this operation is relatively fast.
          val allData = oldData.union(newData)
          dedupe(allData)
         }

      saveCookieSyncLogs(deduped, p2aOutputPath, "syncid")
      saveStats()

      def saveStats() = {
        import spark.implicits._
        Seq(
          Stat(measure = "newDataFull",    value = newDataFull.count()),
          Stat(measure = "newDataDeduped", value = newData.count()),
          Stat(measure = "oldDataCount",   value = oldDataSize),
          Stat(measure = "finalData",      value = deduped.count())
        ).toDF()
          .repartition(1)
          .write
            .mode(SaveMode.Append)
            .json(outputPath + "/Stats/" + now)
      }

  }

  /**
    * dedupe records (syncid, pn_cid) -> ab_cid.
    * last one wins.
    * @param data
    * @return
    */
    def dedupe(data: DataFrame) : DataFrame = {
      data
        .withColumn(
          "row_number",
          row_number.over(
            Window
              .partitionBy("syncid", "pn_cid")  // grouping
              .orderBy(
                  desc("last_update"),          // oldest record wins
                  asc("ab_cid")))               // not very useful, but makes it deterministic
        )
        .where("row_number < 2")
        .drop("row_number")
    }

  /**
    * dedupe records (syncid, pn_cid) -> ab_cid.
    * significant perf improvement - if there is no range in the dates,
    * groupby is equivalent to windowing by date, but cheaper.
    * an arbitrary record wins (no timestamp, so we sort by ab_cid for determinism)
    * @param data
    * @return
    */
    def dedupeSingleDate(data: DataFrame) : DataFrame = {
      data
        .groupBy("syncid", "pn_cid")
        .agg(
          max("ab_cid").as("ab_cid"),
          first("last_update").as("last_update"))
    }

  /**
    * reads cookie sync logs from the list of directories with the range of dates.
    * each date is added to the end of each direcrory, and all the files in this direcrory are read.
    * the schema is hard-coded.
    *
    * @param dateFrom               starting day of the date range
    * @param dateTo                 ending day of the date range, inclusive
    * @param failOnEmptyDirectories if false, empty or corrupt directories will be skipped. For example, missing dates.
    * @param dirs                   list of direcrories
    * @param spark                  implicit
    * @return                       union of all the files
    */
  def readCookieSyncLogs (dateFrom: String, dateTo: String, failOnEmptyDirectories: Boolean, dirs: String*)(implicit spark: SparkSession) : DataFrame = {
    val schema =
      StructType(
          StructField("syncid", StringType, true) ::
          StructField("pn_cid", StringType, true) ::
          StructField("ab_cid", StringType, true) ::
          Nil)
    avro.readAvroFiles(schema, dateFrom, dateTo, failOnEmptyDirectories,  dirs: _*)
  }

  /**
    * Saves processed cookie sync logs state into the output direcrory (with appended date).
    * Saved as Parquet files.
    * If there are files there already, they'll be deleted first.
    * The caller is responsible for moving the logs to /Latest directory
    *   (due to limitation of spark sql - it cannot override its own input)
    * @param data               processed logs
    * @param outputPath         the root directory of the state outpot
    * @param partitionColNames  which columns to use for partitioning, typically syncid.
    */
  def saveCookieSyncLogs(data: DataFrame, outputPath: String, partitionColNames: String*) : Unit = {
    data.write
        .mode(SaveMode.Overwrite)
        .partitionBy(partitionColNames: _*)
        .parquet(outputPath + "/" + now)
   }

  // just do it once
  val now = DateTimeFormat.forPattern("yyyy-MM-dd").print(LocalDate.now())
}

// has to be top level class
case class Stat (timestamp: String = LocalDateTime.now().toString, measure: String, value: Long)

