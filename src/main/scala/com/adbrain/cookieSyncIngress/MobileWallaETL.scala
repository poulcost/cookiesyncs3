package com.adbrain.mobileWalla

import org.apache.spark.sql._
import org.apache.spark.sql.functions._
/**
  * Created by poulcostinsky on 8/8/16.
  */
object MobileWallaETL {

  def readMobileWalla (dirs: String*) (implicit spark: SparkSession ) : DataFrame = {
    val data = spark.read.json(dirs: _*)
    //data.printSchema()
    val grouped = data.select("uiid", "uiidType", "ip", "ts").withColumn("minutes", data("ts").divide(60*60*1000)) // milliseconds to hours

    grouped.createOrReplaceTempView("mob")

    val stats = spark.sql(
      """
        SELECT
          uiid,
          uiidType,
          ip,
          minutes,
          COUNT(*) AS count
        FROM mob
        GROUP BY
          uiid,
          uiidType,
          ip,
          minutes
      """)

    val ocount = data.count()
    val gcount = stats.count()
    println(s"original=$ocount grouped=$gcount ratio=${ocount/gcount}")

    data
  }
  /*
  root
 |-- asn: string (nullable = true)
 |-- carrier: string (nullable = true)
 |-- city: string (nullable = true)
 |-- connectionType: string (nullable = true)
 |-- country: string (nullable = true)
 |-- deviceCategory: string (nullable = true)
 |-- deviceName: string (nullable = true)
 |-- dma: string (nullable = true)
 |-- ip: string (nullable = true)
 |-- latitude: double (nullable = true)
 |-- longitude: double (nullable = true)
 |-- majorOs: string (nullable = true)
 |-- platform: string (nullable = true)
 |-- state: string (nullable = true)
 |-- ts: long (nullable = true)
 |-- uiid: string (nullable = true)
 |-- uiidType: string (nullable = true)
 |-- userAgent: string (nullable = true)
 |-- zip: string (nullable = true)
   */
}
