package com.adbrain.scalaproject

import org.apache.spark.sql.SparkSession
import com.adbrain.cookieSyncIngress._
import com.adbrain.mobileWalla._

// how to run spark sql from intellij: https://www.linkedin.com/pulse/develop-apache-spark-apps-intellij-idea-windows-os-samuel-yee

object scalaproject {

  def main(args: Array[String]): Unit =
  {
    implicit val spark = SparkSession
        .builder()
        .appName("CookieSyncImport")
        .config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
        .config("spark.master", "local[*]")
        .getOrCreate()

    spark.sparkContext.setLogLevel("WARN")
    //cookieSync()
    mobileWalla()

  }

  def mobileWalla() (implicit spark: SparkSession ) = {
    MobileWallaETL.readMobileWalla("/Users/poulcostinsky/Desktop/temp/mob/*.*")
  }
  def cookieSync() (implicit spark: SparkSession ) = {
    CookieSyncImporter.Import(
      "/Users/poulcostinsky/Desktop/cookies/output/",
      false,
      90,
      "2016-07-26",
      "2016-08-04",
      true,
      "s3n://adbrain-external/iponweb/avro/match"
      //"/Users/poulcostinsky/Desktop/cookies/avro/dc",
      //"/Users/poulcostinsky/Desktop/cookies/avro/ipw"
    )

  }
}

/*
"2016-07-26",
"2016-07-26",
oldData=0 newData=402395 totalData=381684 deduped=20711.

"2016-07-26",
"2016-07-27",
ldData=0 newData=607980 totalData=563172 deduped=44808.

*/
