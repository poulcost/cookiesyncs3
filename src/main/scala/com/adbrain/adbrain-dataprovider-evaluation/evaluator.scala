package com.adbrain.dataProviderEvaluator

import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.storage.StorageLevel
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat
/**
  * Created by poulcostinsky on 8/8/16.
  */
class Evaluator (
                files: String,
                statsOutput: String,
                format: String = "json",
                uiid: String = "uiid",
                uiidType: String = "uiidType",
                ip: String = "ip",
                timestamp: String = "ts",
                timestampSecondDivider: Int = 1000,
                histogramColumns: String = ""
                ){

  def read () (implicit spark: SparkSession ) : (DataFrame, DataFrame) = {
    val data = spark.read.format(format).load(files)
    data.printSchema()
    data.describe().show()

    val standardized = data.select(
      data(uiid).as("uiid"),
      data(uiidType).as("uiidType"),
      data(ip).as("ip"),
      data(timestamp).as("timestamp")
    )

    (data, standardized)
  }

  def process() (implicit spark: SparkSession ) : Unit = {
    val data = read()
    val raw = data._1
    val standard = data._2

    val s = stats(standard)
    val h = histogram(raw)

    Seq (("stats", s), ("histogram", h))
        .map(stat => save(stat._1, stat._2))
    // TODO: crosstab
  }

  def histogram(data: DataFrame) : DataFrame = {
    histogramColumns
      .split(',')
      .map(column => histogram(data, column))
      .reduce(_ union _)
  }

  def histogram(data: DataFrame, column: String) : DataFrame = {
    val g = data
      .groupBy(column).agg(count(column).as("count"))
      .withColumn("column", lit(column))

    g.select(
      g("column"),
      g(column).as("value"),
      g("count"))
      .orderBy("column", "value", "count")
  }


  def stats(data: DataFrame) : DataFrame = {
    val standards =
      Seq("total", uiid, uiidType, ip)
        .map(columnName => data.agg((
            if (columnName == "total")
              count(data("uiid"))
            else countDistinct(data(columnName)))
              .as("value"))
          .withColumn("stat",
              lit(columnName + "Count")))
          .reduce(_ union _)

    val idsPerIp =
      data
        .groupBy(ip)
        .agg(countDistinct(uiid).as("count"))
    val ipStats =
      Seq(
        ("count == 1", "1"),
        ("count == 2", "2"),
        ("count > 2 and count < 5", "3..4"),
        ("count > 4 and count < 101", "5..100"),
        ("count > 100", "100+"))
        .map(c => idsPerIp.where(c._1)
            .agg(count("count").as("value"))
            .withColumn("stat", lit("ID per IP: " + c._2)))
        .reduce(_ union _)

    Seq(standards, ipStats).reduce(_ union _)
  }

  def save(statName: String, statValue: DataFrame) : Unit = {
    statValue.show(100)

    statValue
      .repartition(1)
        .write
        .mode(SaveMode.Overwrite)
        .json(statsOutput + "/" + now + "/" + statName + ".json")
  }

  val now = DateTimeFormat.forPattern("yyyy-MM-dd").print(LocalDate.now())
}
