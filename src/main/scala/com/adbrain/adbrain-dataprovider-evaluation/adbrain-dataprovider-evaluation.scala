package com.adbrain.dataProviderEvaluation

import org.apache.spark.sql.SparkSession
import com.adbrain.dataProviderEvaluator

// how to run spark sql from intellij: https://www.linkedin.com/pulse/develop-apache-spark-apps-intellij-idea-windows-os-samuel-yee

object dataProviderEvaluation {

  def main(args: Array[String]): Unit =
  {
    implicit val spark = SparkSession
        .builder()
        .appName("dataProviderEvaluation")
        .config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
        .config("spark.master", "local[*]")
        .getOrCreate()

    spark.sparkContext.setLogLevel("WARN")
    val files = "/Users/poulcostinsky/Desktop/temp/*.gz"
    val stats = "/Users/poulcostinsky/Desktop/temp/Evaluator"
    val ev = new dataProviderEvaluator.Evaluator(
      files,
      stats,
      histogramColumns = "uiidType,connectionType,deviceCategory,majorOs,platform"
      );
    ev.process()
  }

}

